import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {Sequelize} from "sequelize-typescript";
import { UserDetails } from './userDetails.model';
import { UserDetailsDto } from './userDetails.dto';
import { UserRole } from './userDetails.model';

@Injectable()
export class UserDetailsService {
    constructor(private sequelize: Sequelize, @InjectModel(UserDetails) private userDetailsModel: typeof UserDetails,) {
    }

    async register(register: UserDetailsDto): Promise<UserDetails> {
        return this.userDetailsModel.create<UserDetails>(register).then((result) => {
            return result;
        });
    }

    async findOne(id): Promise<UserDetails> {
        return this.userDetailsModel.findOne(id);
    }

    async findRole(id): Promise<UserRole> {
        const userDetails = await this.userDetailsModel.findOne({where: {id: id}});
        return userDetails.role;
    }

    async delete(id: string): Promise<void> {
        await this.userDetailsModel.destroy({where: {id: id}});
    }

    async deleteByUserId(id: string): Promise<void> {
        await this.userDetailsModel.destroy({where: {id: id}});
    }
}