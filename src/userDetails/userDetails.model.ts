import { Column, DataType, Default, HasOne, Model, Table } from 'sequelize-typescript';
import { User } from '../users/user.model';

export enum UserRole {
  Admin = 'Admin',
  User = 'User',
}

@Table
export class UserDetails extends Model {
  @Column
  name: string;

  @Column
  surname: string;

  @Column
  phone: string;

  @Default('User')
  @Column({ type: DataType.ENUM({ values: Object.keys(UserRole) }) })
  role: UserRole;

  @HasOne(() => User)
  user: User
}