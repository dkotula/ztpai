import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { UserDetails } from './userDetails.model';
import { UserDetailsService } from './userDetails.service';

@Module({
  imports: [SequelizeModule.forFeature([UserDetails])],
  providers: [UserDetailsService],
  exports: [SequelizeModule, UserDetailsService]
})
export class UserDetailsModule {}