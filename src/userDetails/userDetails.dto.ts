import { UserRole } from './userDetails.model';

export class UserDetailsDto {
  readonly name: string;
  readonly surname: string;
  readonly phone: string;
  readonly role: UserRole;
}