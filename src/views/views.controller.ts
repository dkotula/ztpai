import { Controller, Get } from '@nestjs/common';
import { ViewsService } from './views.service';

@Controller('views')
export class ViewsController {
  constructor(private readonly viewsService: ViewsService) {}

  @Get()
  getHello(): string {
    return this.viewsService.getHello();
  }
}