import { Column, HasMany, Model, Table } from 'sequelize-typescript';
import { Task } from '../tasks/task.model';

@Table
export class Category extends Model {
  @Column
  category_name: string

  @HasMany(() => Task)
  tasks: Task[]
}