import { Body, Controller, Get, Post } from '@nestjs/common';
import {CategoriesService} from "./categories.service";
import {Category} from "./category.model";

@Controller('categories')
export class CategoriesController {
    constructor(private readonly categoriesService: CategoriesService) {}

    @Get('findAll')
    async findAll(): Promise<Category[]> {
        return this.categoriesService.findAll();
    }

    @Post('add')
    async add(@Body() body): Promise<Category> {
        return this.categoriesService.add(body.category);
    }
}
