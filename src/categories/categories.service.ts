import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {Category} from "./category.model";
import {Sequelize} from "sequelize-typescript";
import { AddDto } from './category.dto';

@Injectable()
export class CategoriesService {
    constructor(private sequelize: Sequelize, @InjectModel(Category) private categoryModel: typeof Category,) {
    }
    async findAll(): Promise<Category[]> {
        return this.categoryModel.findAll();
    }

    async add(add: AddDto): Promise<Category> {
        return await this.categoryModel.create<Category>(add);
    }
}
