import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AddController } from './add/add.controller';
import { ViewsController } from './views/views.controller';
import { GroupsController } from './groups/groups.controller';
import { SettingsController } from './settings/settings.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './users/user.model';
import { UserDetails } from './userDetails/userDetails.model';
import { UserGroup } from './userGroups/userGroup.model';
import { Group } from './groups/group.model';
import { Matrix } from './matrices/matrix.model';
import { MatrixTask } from './matrixTasks/matrixTask.model';
import { Task } from './tasks/task.model';
import { Category } from './categories/category.model';
import { ViewsService } from './views/views.service';
import { UsersService } from './users/users.service';
import { UsersController } from './users/users.controller';
import {UsersModule} from "./users/users.module";
import { CategoriesModule } from './categories/categories.module';
import { TasksModule } from './tasks/tasks.module';
import { AuthModule } from './auth/auth.module';
import { UserDetailsModule } from './userDetails/userDetails.module';
import { UserDetailsService } from './userDetails/userDetails.service';
import { ConfigModule } from '@nestjs/config';
import { MatrixModule } from './matrices/matrix.module';
import { MatrixService } from './matrices/matrix.service';

ConfigModule.forRoot();

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_DATABASE,
      models: [User, UserDetails, UserGroup, Group, Matrix, MatrixTask, Task, Category],
      dialectOptions: {
        "ssl": true
      }
    }),
      UsersModule,
      UserDetailsModule,
      CategoriesModule,
      TasksModule,
      AuthModule,
      MatrixModule
  ],
  controllers: [AppController, AddController, ViewsController, GroupsController, SettingsController, UsersController],
  providers: [AppService, ViewsService, UsersService, UserDetailsService, MatrixService],
})
export class AppModule {}
