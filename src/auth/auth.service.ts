import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserDetailsService } from '../userDetails/userDetails.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private userDetailsService: UserDetailsService,
    private jwtService: JwtService
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    let user = await this.usersService.findOne(email);
    if (user) {
      const isMatch = await bcrypt.compare(pass, user.password);
      if(!isMatch) {
        return {err: "Wrong password!"};
      }
      const userRole = await this.userDetailsService.findRole(user.id_user_details);
      let result = { user, userRole };
      return result;
    }
    return {err: "User does not exist!"};
  }

  async login(user: any) {
    const userValidated = await this.validateUser(user.email, user.password);
    if(userValidated.err)
      return userValidated;
    const payload = { user: {"email": user.email, "role": userValidated.userRole} };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}