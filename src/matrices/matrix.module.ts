import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { MatrixService } from './matrix.service';
import { Matrix } from './matrix.model';

@Module({
  imports: [SequelizeModule.forFeature([Matrix])],
  providers: [MatrixService],
  exports: [SequelizeModule, MatrixService]
})
export class MatrixModule {}