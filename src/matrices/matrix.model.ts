import { BelongsToMany, Column, HasOne, Model, Table } from 'sequelize-typescript';
import { User } from '../users/user.model';
import { MatrixTask } from '../matrixTasks/matrixTask.model';
import { Task } from '../tasks/task.model';

@Table
export class Matrix extends Model {
  @Column({ defaultValue: false })
  public: boolean

  @HasOne(() => User)
  user: User

  @BelongsToMany(() => Task, () => MatrixTask)
  tasks: Task[]
}