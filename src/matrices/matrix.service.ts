import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {Sequelize} from "sequelize-typescript";
import { Matrix } from './matrix.model';

@Injectable()
export class MatrixService {
    constructor(private sequelize: Sequelize, @InjectModel(Matrix) private matrixModel: typeof Matrix,) {
    }

    async create(): Promise<Matrix> {
        return this.matrixModel.create().then((result) => {
            return result;
        });
    }

    async delete(id: string): Promise<void> {
        await this.matrixModel.destroy({where: {id: id}});
    }
}