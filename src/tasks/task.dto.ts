export class TaskDto {
  readonly category_id: number;
  readonly title: string;
  readonly deadline: Date;
  readonly making_time: number;
  readonly priority: number;
  readonly description: string;
}