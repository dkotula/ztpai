import { Module } from '@nestjs/common';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';
import {SequelizeModule} from "@nestjs/sequelize";
import {Task} from "./task.model";
import { UsersModule } from '../users/users.module';

@Module({
  imports: [SequelizeModule.forFeature([Task]), UsersModule],
  controllers: [TasksController],
  providers: [TasksService],
  exports: [SequelizeModule]
})
export class TasksModule {}