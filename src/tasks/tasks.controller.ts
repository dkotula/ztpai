import { Body, Controller, Delete, Get, Param, Post, Put, Request, UseGuards } from '@nestjs/common';
import {TasksService} from "./tasks.service";
import {Task} from "./task.model";
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UsersService } from '../users/users.service';

@Controller('tasks')
export class TasksController {
    constructor(private readonly tasksService: TasksService, private readonly usersService: UsersService) {}

    @UseGuards(JwtAuthGuard)
    @Get('findAll')
    async findAll(@Request() req): Promise<Task[]> {
        const user = await this.usersService.findOne(req.user.user.email);
        return this.tasksService.findAll(user.id_matrix);
    }

    @UseGuards(JwtAuthGuard)
    @Post('add')
    async add(@Body() body, @Request() req): Promise<Task> {
        return this.tasksService.add(body.state, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Delete('delete:id')
    async delete(@Param('id') id: string) {
        return this.tasksService.delete(id);
    }

    @UseGuards(JwtAuthGuard)
    @Put('change:id')
    async change(@Param('id') id: string, @Body() body, @Request() req): Promise<Task> {
        return this.tasksService.change(id, body.state);
    }
}
