import { Body, Injectable, NotFoundException, Param, Request  } from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {Task} from "./task.model";
import {Sequelize} from "sequelize-typescript";
import { TaskDto } from './task.dto';
import { MatrixTask } from '../matrixTasks/matrixTask.model';
import { User } from '../users/user.model';

@Injectable()
export class TasksService {

    constructor(private sequelize: Sequelize, @InjectModel(Task) private taskModel: typeof Task,) {
    }

    async findAll(id_matrix): Promise<Task[]> {
        let taskList = [];
        const tasks = await this.sequelize.getRepository(MatrixTask).findAll({where: {id_matrix: id_matrix}});
        for (const task of tasks) {
            const el = await this.sequelize.getRepository(Task).findOne({where: {id: task.id_task}});
            taskList.push(el);
        }
        return taskList;
    }

    async findOne(id): Promise<Task> {
        return this.taskModel.findOne(id);
    }

    async add(add: TaskDto, userEmail): Promise<Task> {
        const task = await this.taskModel.create<Task>(add);
        const user = await this.sequelize.getRepository(User).findOne({where: {email: userEmail.user.email}});
        await this.sequelize.getRepository(MatrixTask).create({id_matrix: user.id_matrix, id_task: task.id});
        return task;
    }

    async delete(id: string): Promise<void> {
        await this.sequelize.getRepository(MatrixTask).destroy({where: {id_task: id}});
        await this.taskModel.destroy({where: {id: id}});
    }

    async change(id: string, put: TaskDto): Promise<Task> {
        const task = await this.taskModel.update(put, {where: {id: id}});
        return null;
    }
}
