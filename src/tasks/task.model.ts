import { BelongsTo, BelongsToMany, Column, CreatedAt, ForeignKey, Model, Table } from 'sequelize-typescript';
import { Matrix } from '../matrices/matrix.model';
import { MatrixTask } from '../matrixTasks/matrixTask.model';
import { Category } from '../categories/category.model';

@Table
export class Task extends Model {
  @BelongsToMany(() => Matrix, () => MatrixTask)
  matrices: Matrix[]

  @ForeignKey(() => Category)
  @Column
  category_id: number

  @BelongsTo(() => Category)
  category: Category

  @Column
  title: string

  @CreatedAt
  created_at: Date;

  @Column
  deadline: Date;

  @Column
  making_time: number

  @Column
  priority: number

  @Column
  description: string
}