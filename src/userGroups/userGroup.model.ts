import { Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { User } from '../users/user.model';
import { Group } from '../groups/group.model';

@Table
export class UserGroup extends Model {
  @ForeignKey(() => User)
  @Column
  id_user: number

  @ForeignKey(() => Group)
  @Column
  id_group: number
}