import { BelongsToMany, Model, Table } from 'sequelize-typescript';
import { User } from '../users/user.model';
import { UserGroup } from '../userGroups/userGroup.model';

@Table
export class Group extends Model {
  @BelongsToMany(() => User, () => UserGroup)
  users: User[]
}