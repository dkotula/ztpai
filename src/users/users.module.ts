import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './user.model';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserDetailsModule } from '../userDetails/userDetails.module';
import { MatrixModule } from '../matrices/matrix.module';


@Module({
    imports: [SequelizeModule.forFeature([User]), UserDetailsModule, MatrixModule],
    providers: [UsersService],
    controllers: [UsersController],
    exports: [SequelizeModule, UsersService]
})
export class UsersModule {}