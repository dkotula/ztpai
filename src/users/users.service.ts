import { Injectable, Request } from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {User} from "./user.model";
import {Sequelize} from "sequelize-typescript";
import { UserDto } from './user.dto';
import { UserDetails } from '../userDetails/userDetails.model';

@Injectable()
export class UsersService {
    constructor(private sequelize: Sequelize, @InjectModel(User) private userModel: typeof User, @InjectModel(User) private userDetailsModel: typeof UserDetails,) {
    }

    async findOne(email: string): Promise<User | undefined> {
        return this.userModel.findOne({
            where: { email: email }
        });
    }

    async findOneById(id: string): Promise<User | undefined> {
        return this.userModel.findOne({
            where: { id: id }
        });
    }

    async register(register: UserDto): Promise<User> {
        return this.userModel.create<User>(register).then((result) => {
            return result;
        });
    }

    async findAll(): Promise<User[] | undefined> {
        return await this.userModel.findAll();
    }

    async delete(id: string): Promise<void> {
        await this.userModel.destroy({where: {id: id}});
    }
}