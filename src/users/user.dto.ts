export class UserDto {
  readonly email: string;
  readonly password: string;
  readonly id_user_details: number;
  readonly id_user_matrix: number;
}