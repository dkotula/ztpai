import {
  BelongsTo,
  BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Sequelize,
  Table,
} from 'sequelize-typescript';
import { UserDetails } from '../userDetails/userDetails.model';
import { UserGroup } from '../userGroups/userGroup.model';
import { Group } from '../groups/group.model';
import { Matrix } from '../matrices/matrix.model';

@Table
export class User extends Model {
  @ForeignKey(() => UserDetails)
  @Column
  id_user_details: number

  @BelongsTo(() => UserDetails)
  userDetails: UserDetails

  @ForeignKey(() => Matrix)
  @Column
  id_matrix: number

  @BelongsTo(() => Matrix)
  matrix: Matrix

  @Column
  password: string;

  @Column
  email: string;

  @BelongsToMany(() => Group, () => UserGroup)
  groups: Group[]
}