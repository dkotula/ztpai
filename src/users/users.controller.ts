import { Body, Controller, Delete, Get, Param, Post, Request, UseGuards } from '@nestjs/common';
import {UsersService} from "./users.service";
import {UserDetailsService} from "../userDetails/userDetails.service"
import * as bcrypt from 'bcrypt';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { User } from './user.model';
import { MatrixService } from '../matrices/matrix.service';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService, private readonly userDetailsService: UserDetailsService, private readonly matrixService: MatrixService) {}

    @UseGuards(JwtAuthGuard)
    @Get('findAll')
    async findAll(@Request() req): Promise<User[]> {
        if (req.user.user.role !== "Admin") {
            return null;
        }
        return this.usersService.findAll();
    }

    @UseGuards(JwtAuthGuard)
    @Delete('delete:id')
    async delete(@Param('id') id: string, @Request() req) {
        if (req.user.user.role !== "Admin") {
            return null;
        }
        const user = await this.usersService.findOneById(id);
        const userDetailsId = user.id_user_details;
        await this.usersService.delete(id);
        return this.userDetailsService.delete(userDetailsId.toString());
    }

    @Post('register')
    async register(@Body() body): Promise<String> {
        if (!body.state.user.email) {
            return "Email cannot be empty";
        }

        if (!body.state.user.password) {
            return "Password cannot be empty";
        }

        if (!body.state.userDetails.name) {
            return "Name field cannot be empty";
        }

        if (!body.state.userDetails.surname) {
            return "Surname field cannot be empty";
        }

        if (!body.state.userDetails.phone) {
            return "Phone number field cannot be empty";
        }

        if (await this.usersService.findOne(body.state.user.email)) {
            return "User already exists";
        }

        const hash = await bcrypt.hash(body.state.user.password, 10);
        body.state.user.password = hash;
        const userDetails = await this.userDetailsService.register(body.state.userDetails);
        if (!userDetails) {
            return "Cannot create User";
        }

        body.state.user.id_user_details = userDetails.id;


        const matrix = await this.matrixService.create();
        if (!matrix) {
            await this.userDetailsService.delete(userDetails.id)
            return "Cannot create User";
        }

        body.state.user.id_matrix = matrix.id;

        if (!await this.usersService.register(body.state.user)) {
            await this.matrixService.delete(matrix.id)
            await this.userDetailsService.delete(userDetails.id)
            return "Cannot create User";
        }
        return "Success";
    }
}
