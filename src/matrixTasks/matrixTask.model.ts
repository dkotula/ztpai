import { Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { Matrix } from '../matrices/matrix.model';
import { Task } from '../tasks/task.model';

@Table
export class MatrixTask extends Model {
  @ForeignKey(() => Matrix)
  @Column
  id_matrix: number

  @ForeignKey(() => Task)
  @Column
  id_task: number
}