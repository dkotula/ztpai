'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Matrix extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Matrix.init({
    public: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Matrix',
  });
  return Matrix;
};