'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGroup extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserGroup.init({
    id_user: DataTypes.NUMBER,
    id_group: DataTypes.NUMBER
  }, {
    sequelize,
    modelName: 'UserGroup',
  });
  return UserGroup;
};