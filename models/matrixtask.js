'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MatrixTask extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  MatrixTask.init({
    id_matrix: DataTypes.Number,
    id_task: DataTypes.Number
  }, {
    sequelize,
    modelName: 'MatrixTask',
  });
  return MatrixTask;
};